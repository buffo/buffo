package com.buffo.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreApplication {

	public static void main(String[] args) {
		//启动器1
		SpringApplication.run(CoreApplication.class, args);
	}

}
