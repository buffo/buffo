package com.buffo.core.Util;

import com.baidu.aip.client.BaseClient;
import com.baidu.aip.http.AipRequest;
import com.buffo.core.constant.ApiConstants;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import java.util.HashMap;

public class AipImageClassifyBase64 extends BaseClient {
    public AipImageClassifyBase64(String appId, String apiKey, String secretKey) {
        super(appId, apiKey, secretKey);
    }

    /**
     * 通用物体识别接口
     * 该请求用于通用物体及场景识别，即对于输入的一张图片（可正常解码，且长宽比适宜），输出图片中的多个物体及场景标签。
     *
     * @param base64Content - 二进制图像数据
     * @param options       - 可选参数对象，key: value都为string类型
     *                      options - options列表:
     *                      baike_num 返回百科信息的结果数，默认不返回
     * @return JSONObject
     */
    public JSONObject advancedGeneralBase(String base64Content, HashMap<String, String> options) {
        AipRequest request = new AipRequest();
        preOperation(request);

        // String base64Content = Base64Util.encode(image);
        if (!StringUtils.isEmpty(base64Content)) {
            base64Content = base64Content.replace("data:image/jpeg;base64,", "");
        }
        request.addBody("image", base64Content);
        if (options != null) {
            request.addBody(options);
        }
        request.setUri(ApiConstants.ADVANCED_GENERAL);
        postOperation(request);
        return requestServer(request);
    }
}
