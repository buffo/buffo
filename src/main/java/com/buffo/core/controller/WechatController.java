package com.buffo.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller("/wechat")
@RestController
public class WechatController {

    @GetMapping(name = "/index")
    public String getWechatIndex(HttpServletRequest request, HttpServletResponse response) {
        return request.getParameter("name");
    }
}
