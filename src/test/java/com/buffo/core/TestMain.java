package com.buffo.core;

import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class TestMain {

    public static void main1(String[] args) {
        Thread thread3 = new Thread(() -> {
            SingletonDemo singletonDemo = SingletonDemo.getInstence();
            System.out.println("thread1:" + JSONObject.toJSONString(singletonDemo));
        });
        Thread thread4 = new Thread(() -> {
            SingletonDemo singletonDemo = SingletonDemo.getInstence();
            System.out.println("thread2:" + JSONObject.toJSONString(singletonDemo));
        });
        thread3.start();
        thread4.start();

        long start = System.currentTimeMillis();
        SafeCalc safeCalc = new SafeCalc();
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 100000000; i++) {
                safeCalc.add();
            }
            System.out.println("thred1--->" + safeCalc.get());
        });
        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 100000000; i++) {
                safeCalc.add();
            }
            System.out.println("thred2--->" + safeCalc.get());
        });
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis() - start;
        System.out.println("res:====>" + safeCalc.get());
        System.out.println("time:====>" + end);
    }

    static void readNIO() {
        String pathname = "/Users/folaibu/Downloads/temp_file/read.txt";
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(new File(pathname));
            FileChannel channel = fin.getChannel();
            int capacity = 100;// 字节
            ByteBuffer bf = ByteBuffer.allocate(capacity);
            int length = -1;
            while ((length = channel.read(bf)) != -1) {
                bf.clear();
                byte[] bytes = bf.array();
                System.out.write(bytes, 0, length);
                System.out.println();
            }
            channel.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static void writeNIO() {
        String filename = "/Users/folaibu/Downloads/temp_file/write.txt";
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(filename));
            FileChannel channel = fos.getChannel();
            ByteBuffer src = Charset.forName("utf8").encode("你好你好你好你好你好");
            int length = 0;
            while ((length = channel.write(src)) != 0) {
                System.out.println("写入长度:" + length);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void sertArr(int[] arr) {
        if (arr.length <= 0) {
            return;
        }
        int i = 0;
        int j = i + 1;
        while (j < arr.length) {
            if (arr[i] < arr[j]) {
                j++;
                i++;
            } else {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;

            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 7, 2, 0, 9, 0, 5};
        sertArr(arr);
        System.out.println(arr.toString());
    }
}
