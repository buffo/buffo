package com.buffo.core;

public class TestDemo {

    public static void main(String[] args) {
        int[] nums = {6, 0, 5, 0, 1, 5,1};
        System.out.println(singleNumber(nums));
    }

    public static int singleNumber(int[] nums) {
        int a = 0;
        for (int num : nums) {
            a = a ^ num;
        }
        return a;
    }
}
