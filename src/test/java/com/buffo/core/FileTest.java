package com.buffo.core;

import java.io.*;

public class FileTest {

    public static void main(String[] args) throws IOException {

        String a = "abc";
        String b = a;
        a=a.toUpperCase();
        System.out.println("a1:"+a+"；b:"+b);
        a=a.replace("a","");
        System.out.println("a2:"+a+"；b:"+b);
        a.isEmpty();
        System.out.println("a4:"+a+"；b:"+b);
        a.codePointAt(2);
        System.out.println("a5:"+a+"；b:"+b);
        a.concat("de");
        System.out.println("a6:"+a+"；b:"+b);
        a.intern();
        System.out.println("a7:"+a+"；b:"+b);
        a.split("a");
        System.out.println("a8:"+a+"；b:"+b);
        a.trim();
        System.out.println("a9:"+a+"；b:"+b);
        a.subSequence(1,2);
        System.out.println("a10:"+a+"；b:"+b);
    }
}
