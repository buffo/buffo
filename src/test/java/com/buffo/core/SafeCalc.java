package com.buffo.core;

public class SafeCalc {
    long value = 0L;

    synchronized long get() {
        return value;
    }

    public synchronized void add() {
        value++;
    }
}
