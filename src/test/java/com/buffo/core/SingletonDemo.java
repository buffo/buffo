package com.buffo.core;

import lombok.Data;

@Data
public class SingletonDemo {
    private String name = "hhhh";
    static volatile boolean flag = true;
    public static SingletonDemo singleton;

    public static SingletonDemo getInstence() {
        if (singleton == null && flag) {
            synchronized (SingletonDemo.class) {
                if (singleton == null && flag) {
                    System.out.println("阿斯达");
                    singleton = new SingletonDemo();
                    flag = false;
                }
            }
        }
        return singleton;
    }

}
