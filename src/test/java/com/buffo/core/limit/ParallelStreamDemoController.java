package com.buffo.core.limit;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import org.junit.Test;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/11/26 19:45
 */
@Controller
@RequestMapping
public class ParallelStreamDemoController {

    @Test
    public void doSome() {
        Stopwatch stopwatch = Stopwatch.createStarted();
        AtomicReference<DemoNo> a = new AtomicReference<>(new DemoNo(1));
        AtomicReference<DemoNo> b = new AtomicReference<>(new DemoNo(1));
        AtomicReference<DemoNo> c = new AtomicReference<>(new DemoNo(1));
        AtomicReference<DemoNo> d = new AtomicReference<>(new DemoNo(1));
        List<Integer> tasks = Collections.unmodifiableList(Arrays.asList(1, 2, 3, 4));
        String finalRequest = "request";
        tasks.parallelStream().forEach(e -> {
            switch (e) {
                case 1:
                    System.out.println(String.format("线程%s进入……", e));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    a.get().setA(2);
                    System.out.println(String.format("线程%s结束……结果:%s--->req:%s", e, JSON.toJSONString(a.get()), finalRequest));
                    break;
                case 2:
                    System.out.println(String.format("线程%s进入……", e));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    b.get().setA(3);
                    System.out.println(String.format("线程%s结束……结果:%s--->req:%s", e, JSON.toJSONString(b.get()), finalRequest));
                    break;
                case 3:
                    System.out.println(String.format("线程%s进入……", e));
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    c.get().setA(4);
                    System.out.println(String.format("线程%s结束……结果:%s--->req:%s", e, JSON.toJSONString(c.get()), finalRequest));
                    break;
                case 4:
                    System.out.println(String.format("线程%s进入……", e));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    d.get().setA(5);
                    System.out.println(String.format("线程%s结束……结果:%s--->req:%s", e, JSON.toJSONString(d.get()), finalRequest));
                    break;
                default:
                    break;
            }
        });
        System.out.println(String.format("结果a:%s", JSON.toJSONString(a)));
        System.out.println(String.format("结果b:%s", JSON.toJSONString(b)));
        System.out.println(String.format("结果c:%s", JSON.toJSONString(c)));
        System.out.println(String.format("结果d:%s", JSON.toJSONString(d)));
        long aa = stopwatch.elapsed(TimeUnit.MILLISECONDS);
        System.out.println(String.format("耗时:%s", aa));
    }

    class DemoNo {
        int a;

        public DemoNo(int a) {
            this.a = a;
        }

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }
    }
}
