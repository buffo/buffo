package com.buffo.core.limit;

import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.RateLimiter;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/11/21 16:27
 */
public class LimitDemo {
    RateLimiter rateLimiter = RateLimiter.create(10);

    private void doTest() {
        if (rateLimiter.tryAcquire()) {
            System.out.println("允许访问");
        } else {
            System.out.println("限流！！！稍后重试");
        }
    }

    public static void main(String[] args) {
//        final CountDownLatch downLatch = new CountDownLatch(2);
//        Random random = new Random();
//        new Thread(() -> {
//            try {
//                System.out.println("子线程"+Thread.currentThread().getName()+"正在执行");
//                Thread.sleep(random.nextInt(5000));
//                System.out.println("线程：" + Thread.currentThread().getName());
//                downLatch.countDown();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, "111111").start();≥
//        new Thread(() -> {
//            try {
//                System.out.println("子线程"+Thread.currentThread().getName()+"正在执行");
//                Thread.sleep(random.nextInt(5000));
//                System.out.println("线程：" + Thread.currentThread().getName());
//                downLatch.countDown();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, "222222").start();
//
//        System.out.println("main thread wait.");
//        try {
//            downLatch.await();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("main thread end...");

        final CountDownLatch countDownLatch = new CountDownLatch(3);
        Stopwatch stopwatch = Stopwatch.createStarted();
        ExecutorService executorService = newFixedThreadPool(5);
        executorService.submit(new Thread(() -> {
            try {
                System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
                Thread.sleep(5000);
                System.out.println("线程：" + Thread.currentThread().getName());
                countDownLatch.countDown();
                System.out.println("任务数：" + countDownLatch.getCount());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "T1"));
        executorService.submit(new Thread(() -> {
            try {
                System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
                Thread.sleep(6000);
                System.out.println("线程：" + Thread.currentThread().getName());
                countDownLatch.countDown();
                System.out.println("任务数：" + countDownLatch.getCount());
                while (countDownLatch.getCount() > 0) {
                    countDownLatch.countDown();
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "T2"));
        System.out.println("main thread wait.");
        try {

            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        System.out.println("main thread end...");
        long sends = stopwatch.elapsed(TimeUnit.SECONDS);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long sends1 = stopwatch.elapsed(TimeUnit.SECONDS);
        System.out.println("时间：" + sends + "|sends1:" + sends1);
    }


}
