package com.buffo.core.limit;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.*;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/11/24 10:52
 */
public class GuavaFuture {
    //定义一个线程池，用于处理所有任务 -- MoreExecutors
    private final static ListeningExecutorService sService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(3));


    /**
     * Futures.transformAsync()支持多个任务链式异步执行，并且后面一个任务可以拿到前面一个任务的结果
     */
    @Test
    public void multiTaskTransformAsyncTest() {

        final CountDownLatch latch = new CountDownLatch(1);

        // 第一个任务
        ListenableFuture<String> task1 = sService.submit(() -> {
            System.out.println("第一个任务开始执行...");
            try {
                Thread.sleep(10 * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "第一个任务的结果";
        });

        // 第二个任务，里面还获取到了第一个任务的结果
        AsyncFunction<String, String> queryFunction = new AsyncFunction<String, String>() {
            public ListenableFuture<String> apply(String input) {
                return sService.submit(new Callable<String>() {
                    public String call() throws Exception {
                        System.out.println("第二个任务开始执行...");
                        try {
                            Thread.sleep(10 * 1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return input + " & 第二个任务的结果 ";
                    }
                });
            }
        };

        // 把第一个任务和第二个任务关联起来
        ListenableFuture<String> first = Futures.transformAsync(task1, queryFunction, sService);

        // 监听返回结果
        Futures.addCallback(first, new FutureCallback<String>() {
            public void onSuccess(String result) {
                System.out.println("结果: " + result);
                latch.countDown();
            }

            public void onFailure(Throwable t) {
                System.out.println(t.getMessage());
                latch.countDown();
            }
        }, MoreExecutors.directExecutor());

        try {
            // 等待所有的线程执行完
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFutureDemo() {
        Stopwatch stopwatch = Stopwatch.createStarted();
        final CountDownLatch latch = new CountDownLatch(2);
        final boolean normal = false;
        ListenableFuture<String> listenableFuture1 = sService.submit(() -> {
            System.out.println("SSS---->" + Thread.currentThread().getName());
            Thread.sleep(1000);
            return "任务1开始。。。。。。";
        });
        Futures.addCallback(listenableFuture1, new FutureCallback<String>() {
            @Override
            public void onSuccess(@Nullable String result) {
                System.out.println("任务1(测试正常执行)结果: 正常执行完成 -- " + result);
                latch.countDown();
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println("任务1(测试正常执行)结果: 抛异常了");
                latch.countDown();
            }
        }, MoreExecutors.directExecutor());

        ListenableFuture<String> listenableFuture2 = sService.submit(() -> {
            System.out.println("SSS---->" + Thread.currentThread().getName());
            Thread.sleep(1000);
            return "任务2开始。。。。。。";
        });
        Futures.addCallback(listenableFuture2, new FutureCallback<String>() {
            @Override
            public void onSuccess(@Nullable String result) {
                System.out.println("任务2(测试正常执行)结果: 正常执行完成 -- " + result);
                latch.countDown();
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println("任务2(测试正常执行)结果: 抛异常了");
                latch.countDown();
            }
        }, MoreExecutors.directExecutor());



        try {
            // 等待所有的线程执行完
            latch.await();
            System.out.println("结果1:"+ JSON.toJSONString(listenableFuture1.get()));
            System.out.println("结果2:"+ JSON.toJSONString(listenableFuture2.get(1,TimeUnit.SECONDS)));
            long s = stopwatch.elapsed(TimeUnit.MILLISECONDS);
            System.out.println("耗时："+s+"|shut:"+sService.isShutdown());
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
