package com.buffo.core.limit;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import org.junit.Test;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.*;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/11/24 10:07
 */
public class FutureDemo {
//    private ExecutorService executorService = Executors.newFixedThreadPool(3);
//
//    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
//        Stopwatch stopwatch = Stopwatch.createStarted();
//        FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                System.out.println("开始-----");
//                Thread.sleep(2000);
//                System.out.println("结束-----");
//                return "null";
//            }
//        });
//        long s1 = stopwatch.elapsed(TimeUnit.MILLISECONDS);
//        System.out.println("哈哈哈哈哈哈s1---->" + s1);
//        new Thread(futureTask).start();
//        long s2 = stopwatch.elapsed(TimeUnit.MILLISECONDS);
//        System.out.println("1111111111s2---->" + s2);
//        String res = futureTask.get(3000, TimeUnit.MILLISECONDS);
//        long s3 = stopwatch.elapsed(TimeUnit.MILLISECONDS);
//        System.out.println("svsdvdvs3---->" + s3);
//        System.out.println("s:" + res);
//    }

    // 1、创建线程池
    private ExecutorService executorService = Executors.newFixedThreadPool(3);

    public static HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        }
        return null;
    }

    @Test
    public void futureTaskDemo() {
        final CountDownLatch latch = new CountDownLatch(2);
        Stopwatch stopwatch = Stopwatch.createStarted();

        System.out.println(JSON.toJSONString(FutureDemo.getRequest()));

        FutureTask<String> task1 = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("任务1-----》");
                Thread.sleep(500);
                latch.countDown();
                return "任务1";
            }
        });
        FutureTask<String> task2 = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("任务2-----》");
                Thread.sleep(300);
                latch.countDown();
                return "任务2";
            }
        });
        executorService.execute(task1);
        executorService.execute(task2);

        String a = null;
        String b = null;
        try {
            latch.await(5000, TimeUnit.MILLISECONDS);
            a = task1.get();
            b = task2.get();
            long s = stopwatch.elapsed(TimeUnit.MILLISECONDS);
            System.out.println("耗时：" + s + "|shut:" + executorService.isShutdown());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("结束1：" + a);
        System.out.println("结束2：" + b);
        System.out.println("shut1111:" + executorService.isShutdown());
        System.out.println("latch:" + latch.getCount());
        long s = stopwatch.elapsed(TimeUnit.MILLISECONDS);
        System.out.println("耗时：" + s + "|shut:" + executorService.isShutdown());
    }

}
