package com.buffo.core.queue;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/12/8 18:02
 */
public class QueueDemo {

    public static void main(String[] args) {
        CircularQueue queue = new CircularQueue(10);
        for (int i = 0; i < 11; i++) {
            System.out.println(queue.enqueue(i));
        }
        System.out.println(queue.toString());
    }
}
