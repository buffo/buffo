package com.buffo.core.queue;

import java.util.Arrays;

/**
 * @Description :循环队列
 * @Author folaibu
 * @Date 2019/12/8 17:33
 */
public class CircularQueue {
    /** 元数据 */
    private Object[] term;
    /** 循环队列大小 */
    private int n = 0;
    /** 头指针 */
    private int head = 0;
    /** 尾指针 */
    private int tail = 0;

    public CircularQueue(int size) {
        if (size > 0) {
            term = new Object[size];
            //循环队列会浪费一个地址空间
            n = size;
        }
    }

    public boolean enqueue(Object object) {
        if ((tail + 1) % n == head) {
            return false;
        }
        term[tail] = object;
        tail = (tail + 1) % n;
        return true;
    }

    public Object dequeue() {
        if (head == tail) {
            return null;
        }
        Object object = term[head];
        head = (head + 1) % n;
        return object;
    }

    @Override
    public String toString() {
        return "CircularQueue{" +
                "term=" + Arrays.toString(term) +
                ", n=" + n +
                ", head=" + head +
                ", tail=" + tail +
                '}';
    }
}
