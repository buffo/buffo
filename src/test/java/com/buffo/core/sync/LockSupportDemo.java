package com.buffo.core.sync;

import java.util.concurrent.locks.LockSupport;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/8/25 17:21
 */
public class LockSupportDemo {
    private static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            synchronized (lock) {
                System.out.println("进入---->" + Thread.currentThread().getName());
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(LockSupport.getBlocker(Thread.currentThread()));
                LockSupport.park();
                if (Thread.currentThread().isAlive()) {
                    System.out.println("线程存活*****" + Thread.currentThread().getName());
                }
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("线程中断");
                }
                System.out.println("继续执行------");
            }
        }, "T1");
        Thread t2 = new Thread(() -> {
            synchronized (lock) {
                System.out.println("进入---->" + Thread.currentThread().getName());
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LockSupport.park();
                if (Thread.currentThread().isAlive()) {
                    System.out.println("线程存活*****" + Thread.currentThread().getName());
                }
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("线程中断");
                }
                System.out.println("继续执行------");
            }
        }, "T2");
        t2.start();
        System.out.println(LockSupport.getBlocker(t2));
        Thread.sleep(1000L);
        LockSupport.unpark(t2);
        t1.start();
        System.out.println("解锁成功");
    }


}
