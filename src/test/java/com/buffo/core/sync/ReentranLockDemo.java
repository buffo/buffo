package com.buffo.core.sync;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019-08-25 10:46
 */
public class ReentranLockDemo extends AbstractQueuedSynchronizer {
    Lock lock = new ReentrantLock();
    private static int code =0;
    public static void main(String[] args) {
        final ReentranLockDemo test = new ReentranLockDemo();
        new Thread(() -> {
            test.insert(Thread.currentThread(),2);
        }, "T1").start();
        new Thread(() -> {
            test.insert(Thread.currentThread(),3);
        }, "T2").start();
        System.out.println(code);
    }

    private void insert(Thread thread,Integer a) {
        lock.lock();
        System.out.println(thread.getName() + "---获取锁");
        try {
            code=a;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            System.out.println(thread.getName() + "释放了锁");
        }
    }
}
