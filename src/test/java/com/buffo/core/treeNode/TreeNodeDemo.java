package com.buffo.core.treeNode;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/9/19 17:56
 */
public class TreeNodeDemo {

    public static void main(String[] args) {
        TreeNode root = new TreeNode('A');
        root.setLeft(new TreeNode('B'));
        root.setRight(new TreeNode('C'));
        root.getLeft().setLeft(new TreeNode('D'));
        root.getLeft().setRight(new TreeNode('E'));
        root.getRight().setLeft(new TreeNode('F'));
        root.getLeft().getRight().setLeft(new TreeNode('G'));
    }
}
