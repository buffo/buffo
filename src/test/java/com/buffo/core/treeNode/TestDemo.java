package com.buffo.core.treeNode;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/8/30 14:49
 */
public class TestDemo {
    static int counter = 0;
    static int counter1 = 0;

    public static void main(String[] args) {
        ThreadPoolExecutor pool = new ThreadPoolExecutor(12, 15, 60, TimeUnit.SECONDS, new LinkedBlockingDeque<>(1));
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(() -> {
                System.out.println("开始：" + Thread.currentThread().getName());
                LockSupport.parkNanos(1000);
                counter++;
                System.out.println("计数：--" + Thread.currentThread().getName() + "--counter:" + counter);
            }, "t" + i);
            t.start();
            System.out.println("00000");
        }
        System.out.println(counter);
        System.out.println("-------------------------------");
//        LockSupport.unpark(Thread.currentThread());
//        for (int i = 10; i < 20; i++) {
//            pool.execute(new DoSome());
//            System.out.println("poolSize:" + pool.getCorePoolSize() + "--" + JSON.toJSONString(pool.getQueue()) + "--" + pool.getCompletedTaskCount());
//        }
//        System.out.println(counter1);
    }

    static class DoSome implements Runnable {

        @Override
        public void run() {
            System.out.println("1111开始：" + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter1++;
            System.out.println("1111计数：--" + Thread.currentThread().getName() + "--counter:" + counter1);
        }
    }
}
