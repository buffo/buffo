package com.buffo.core.treeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/9/19 18:03
 */
public class TreeNodeCreator {

    public <T extends DemoVo> void setA(List<T> z) {
        return;
    }

    public void doSome() {
        List<ChildDemoVo> clist = new ArrayList<>();
        ChildDemoVo c = new ChildDemoVo();
        clist.add(c);
        setA(clist);
        List<DemoVo> ds = new ArrayList<>();
        DemoVo d = new DemoVo();
        ds.add(d);
        setA(ds);
    }

}
