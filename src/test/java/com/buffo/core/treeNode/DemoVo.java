package com.buffo.core.treeNode;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/9/20 17:58
 */
public class DemoVo {
    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
