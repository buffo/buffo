package com.buffo.core.treeNode;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/9/19 17:47
 */
public class TreeNode {
    private final char nodeValue;
    private TreeNode parent;
    private TreeNode left;
    private TreeNode right;

    public TreeNode(char nodeValue) {
        this.nodeValue = nodeValue;
        this.right = null;
        this.left = null;
        this.parent = null;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
        if (left != null) {
            this.left.setParent(this);
        }
    }

    public void setRight(TreeNode right) {
        this.right = right;
        if (right != null) {
            this.right.setParent(this);
        }
    }

    private void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public TreeNode getLeft() {
        return left;
    }

    public TreeNode getRight() {
        return right;
    }

    public TreeNode getParent() {
        return parent;
    }

    public char getNodeValue() {
        return nodeValue;
    }
}
