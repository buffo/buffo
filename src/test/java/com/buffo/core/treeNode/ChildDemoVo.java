package com.buffo.core.treeNode;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/9/20 17:58
 */
public class ChildDemoVo extends DemoVo {
    private String age;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
