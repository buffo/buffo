package com.buffo.core.demo;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019-08-23 18:01
 */
public class SomeOne extends Play implements Learn {

    @Override
    public String learn(String s) {
        System.out.println("-----learn---");
        return null;
    }

    @Override
    public void sleep() {
        System.out.println("-----sleep---");
    }

    @Override
    public String teach(String a) {
        System.out.println("------teach--");
        return null;
    }

    @Override
    public void play(String s) {

    }

    @Override
    public void toDo() {

    }

    @Override
    public void toSee() {

    }
}
