package com.buffo.core.demo;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/8/25 16:20
 */
public interface Sleep {
    void sleep();
}
