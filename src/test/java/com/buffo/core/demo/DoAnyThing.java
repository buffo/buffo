package com.buffo.core.demo;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/8/25 16:04
 */
public abstract class DoAnyThing implements Learn,Teach,Sleep {
    public abstract void toDo();
    public abstract void toSee();
}
