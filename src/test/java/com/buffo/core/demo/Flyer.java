package com.buffo.core.demo;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/12/13 12:45
 */
public interface Flyer {
    boolean fly(String name);
}
