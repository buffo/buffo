package com.buffo.core.demo;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019/12/13 12:32
 */
public interface Jump {
    boolean jump(String name);
}
