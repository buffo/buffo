package com.buffo.core.demo;

/**
 * @Description :
 * @Author folaibu
 * @Date 2019-08-23 17:53
 */
public interface Teach {
    String teach(String a);
}
